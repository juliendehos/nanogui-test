{ stdenv, fetchFromGitHub, cmake, x11, xorg, libGL }:

stdenv.mkDerivation {

  name = "nanogui";

  src = fetchFromGitHub {
    owner = "wjakob";
    repo = "nanogui";
    #rev = "7c9cfd360af9d92937b1ea86c8d24f2235efde08";
    #sha256 = "1gql1dnvc6ds84k1grpfxhyhl83fhp77156pv0g5f009xfyq9p7p";
    rev = "a6ae9084b3def9a43ccdc42d0dbf6a353c63c26d";
    sha256 = "1n6dkjc300amwl1m6cyfdcc2nd4f6l4zc5ffscbxfnh7hf2h5kp0";
    fetchSubmodules = true;
  };

  buildInputs = [
    cmake
    x11
    xorg.libXrandr
    xorg.libXinerama
    xorg.libXcursor
    xorg.libXi
    xorg.libXxf86vm
    libGL
  ];

}

