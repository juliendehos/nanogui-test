{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation {

  name = "nanovg";

  src = fetchFromGitHub {
    owner = "memononen";
    repo = "nanovg";
    rev = "1f9c8864fc556a1be4d4bf1d6bfe20cde25734b4";
    sha256 = "08r15zrr6p1kxigxzxrg5rgya7wwbdx7d078r362qbkmws83wk27";
  };

  buildPhase = ''
    gcc -shared -fPIC -o libnanovg.so src/*.c
  '';

  installPhase = ''
    mkdir -p $out/{lib,include}
    cp $src/src/*.h $out/include/
    cp libnanovg.so $out/lib/
  '';

}

