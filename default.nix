with import <nixpkgs> {};

stdenv.mkDerivation {

  name = "nanogui-test";

  src = ./.;

  installPhase = ''
    mkdir -p $out/bin
    cp *.out $out/bin/
    cp -R resources $out/bin/
  '';

  buildInputs = [
    (callPackage ./nix/nanogui.nix {})
    (callPackage ./nix/nanovg.nix {})

    eigen
    glfw
    pkgconfig
  ];

}

