BIN= example1.out example2.out example3.out example4.out

all: $(BIN)

%.out: src/%.cpp
	g++ -o $@ $< `pkg-config --cflags --libs eigen3 glfw3` -lnanovg -lnanogui -lGL

clean:
	rm -f $(BIN)

